/****************************************************************************/

#include <math.h>

#include <iostream>

#include <simplecat/Master.h>
#include <simplecat/Elmo/Elmo.h>

simplecat::Master master;
simplecat::Elmo_GoldWhistle elmo_gw;

unsigned int control_frequency = 1000; // Hz

/****************************************************************************/

void ctrl_c_handler(int s)
{
    std::cout << "exiting" << std::endl;
    master.stop();
}

/****************************************************************************/

const int    sinusoid_amplitude = 1000;
const double sinusoid_freq = 0.1;

const double Kp = 2.0; // proportional gain
const double Kd = 2.8; // derivative gain
                       // critical unit mass damping Kd = 2*sqrt(Kp)

const int encoder_resolution = 512;
bool initialized = false;
int pos_offset = 0;
int last_pos = 0;

unsigned int loop_counter = 0;
void control_callback()
{
    // PD Control
    if (initialized==false && elmo_gw.initialized()){
       pos_offset  = elmo_gw.position_;
       last_pos    = 0;
       initialized = true;

       std::cout << "================\n";
       std::cout << "initial position: " << elmo_gw.position_ << '\n';
    }

    double t = ((double)(loop_counter))/control_frequency;
    int desired_pos = sinusoid_amplitude*sin(2*M_PI*sinusoid_freq*t);
    int zeroed_pos = (elmo_gw.position_-pos_offset);
    int est_vel = (zeroed_pos - last_pos);
    elmo_gw.target_torque_ = Kp*(desired_pos - zeroed_pos) - Kd*est_vel;
    last_pos = zeroed_pos;

    // print
    if (loop_counter%500==0)
    {
        std::cout << std::dec;
        std::cout << "desired  position: " << desired_pos << '\n';
        std::cout << "zeroed   position: " << elmo_gw.position_-pos_offset << '\n';
        std::cout << "actual   position: " << elmo_gw.position_ << '\n';
        std::cout << "estimate velocity: " << est_vel << '\n';
        std::cout << "maximum  torque  : " << elmo_gw.max_torque_ << '\n';
        std::cout << "desired  torque  : " << elmo_gw.target_torque_ << '\n';
        std::cout << "actual   torque  : " << elmo_gw.torque_ << '\n';
        for (int i=0; i<6; ++i){
            std::cout << "digital input " << i << ": " << elmo_gw.digital_inputs_[i] << '\n';
        }
    }

    ++loop_counter;
}

/****************************************************************************/

int main(int argc, char **argv)
{
    master.setCtrlCHandler(ctrl_c_handler);

    master.addSlave(0,0,&elmo_gw);

    master.setThreadHighPriority();
    //master.setThreadRealTime();

    master.activate();

    master.run(control_callback, control_frequency);


    std::cout << "run time : " << master.elapsedTime() << std::endl;
    std::cout << "updates  : " << master.elapsedCycles() << std::endl;
    std::cout << "frequency: " << master.elapsedCycles()/master.elapsedTime() << std::endl;

    return 0;
}

/****************************************************************************/


