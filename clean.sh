rm -rf build
rm -rf lib
cd examples
rm -rf bin
rm -rf beckhoff_analog_read_write/build
rm -rf elmo_gold_whistle/build
rm -rf shared_memory/control_loop/build
rm -rf shared_memory/data_logger/build
rm -rf tcp_server/control_loop/build
rm -rf tcp_server/data_logger/build
cd ..
